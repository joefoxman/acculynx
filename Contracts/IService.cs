﻿namespace TestApi.Contracts {
    public interface IService<T> : IServiceInterfaceBase {
        Task<T> GetData(string data);
    }
}