﻿using TestApi.Shared.StackOverflow;

namespace TestApi.Contracts {
    public interface IStackExchangeService : IServiceInterfaceBase {
        Task<IEnumerable<QuestionInfo>> GetAnswers();
    }
}