# WebAPI

## Name
Test WepAPI project.

## Description
This project will show how you can call an API backend from a Vue.js front end
You can see today's weather based on a zipcode and your Geolocation will return your IP address along with City and State.
You can also query Stack Exchange for questions with at least 2 answers that have been accepted.
If you click on a question it will show all the answers and you can check the one you think is correct.

## Visuals
This application is using Vue 2.x along with Vuetify front end framework.

## Installation
Clone this repo locally.
Run npm install from the web\clientapp folder
Run project from Visual Studio 2022

## Authors and acknowledgment
This project was built by Joey Fox.
