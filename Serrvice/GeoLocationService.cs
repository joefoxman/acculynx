﻿using TestApi.Contracts;
using TestApi.Shared;

namespace TestApi.Services {
    public class GeoLocationService : IGeoLocationService, IServiceInterfaceBase {
        private string _url;
        private readonly IApiService _apiService;

        public GeoLocationService(IApiService apiService, CommonSettings commonSettings) {
            _apiService = apiService;
            _url = $"{commonSettings.GetLocationServiceUrl}";
        }

        public async Task<GeoLocation> GetData(string data) {
            if (string.IsNullOrWhiteSpace(data)) {
                data = string.Empty;
            }
            _url = _url.Replace("{ipAddress}", $"&ipAddress={data}");
            var model = await _apiService.GetData<GeoLocation>(_url);
            // this random delay similates a wait period
            var randomDelayMilliseconds = Helper.GetRandomDelayMilliseconds();
            Console.WriteLine($"GetLocation GetData Delayed by {randomDelayMilliseconds} milliseconds");
            await Task.Delay(randomDelayMilliseconds);
            return model;
        }
    }
}