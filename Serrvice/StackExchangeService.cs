﻿using TestApi.Contracts;
using TestApi.Shared;
using TestApi.Shared.StackOverflow;

namespace TestApi.Services {
    public class StackExchangeService : IStackExchangeService, IServiceInterfaceBase {
        private readonly string _stackExchangeUrl;
        private readonly string _stackExchangeQuestionsUrl;
        private readonly string _stackExchangeQuestionWithAllAnswers;
        private readonly IApiService _apiService;

        public StackExchangeService(
            IApiService apiService, 
            CommonSettings commonSettings) {
            
            _apiService = apiService;
            _stackExchangeUrl = commonSettings.StackExchangeUrl;
            _stackExchangeQuestionsUrl = commonSettings.StackExchangeQuestionsUrl;
            _stackExchangeQuestionWithAllAnswers = commonSettings.StackExchangeQuestionWithAllAnswers;
        }
        private async Task<IEnumerable<QuestionInfo>> GetQuestions() {
            var url = _stackExchangeUrl + _stackExchangeQuestionsUrl;
            var questionWrapper = await _apiService.GetData<QuestionWrapper>(url);
            var questions = new List<QuestionInfo>();

            if (questionWrapper != null && questionWrapper.Items != null && questionWrapper.Items.Any()) {
                // map list of questions inside wrapper. We can also do this with automapper when there is more time
                foreach (var question in questionWrapper.Items) {
                    questions.Add(new QuestionInfo {
                        CreationDate = question.CreationDate,
                        QuestionId = question.QuestionId,
                        LastActivityDate = question.LastActivityDate,
                        AcceptedAnswerId = question.AcceptedAnswerId,
                        AnswerCount = question.AnswerCount,
                        IsAnswered = question.IsAnswered,
                        Link = question.Link,
                        Title = question.Title,
                    });
                }
            }
            return questions;
        }
        public async Task<IEnumerable<QuestionInfo>> GetAnswers() {
            var questions = await GetQuestions();
            var returnQuestionWithAnswers = new List<QuestionInfo>();

            foreach (var question in questions) {
                var url = _stackExchangeUrl + _stackExchangeQuestionWithAllAnswers.Replace("{questionId}", question.QuestionId.ToString());
                var answerWrapper = await _apiService.GetData<AnswerWrapper>(url);

                // skip this questions because it has no answers or only one answer or is not answered
                if (question.AnswerCount <= 1 || !question.IsAnswered) {
                    continue;
                }

                if (answerWrapper != null && answerWrapper.Items != null && answerWrapper.Items.Any()) {
                    var returnQuestion = new QuestionInfo {
                        QuestionId = question.QuestionId,
                        AcceptedAnswerId = question.AcceptedAnswerId,
                        AnswerCount = question.AnswerCount,
                        IsAnswered=question.IsAnswered,
                        Title = question.Title,
                        Link = question.Link,
                        CreationDate = question.CreationDate,
                        LastActivityDate = question.LastActivityDate,
                        Answers = new List<AnswerInfo>()
                    };

                    foreach (var answer in answerWrapper.Items) {
                        var questionAnswer = new AnswerInfo {
                            AnswerId = answer.AnswerId,
                            ContentLicense = answer.ContentLicense,
                            CreationDate = answer.CreationDate,
                            IsAccepted = answer.IsAccepted,
                            LastActivityDate= answer.LastActivityDate,
                            LastEditDate = answer.LastEditDate,
                            QuestionId = answer.QuestionId,
                            Score = answer.Score,
                            Title = answer.Title,
                            Body = answer.Body,
                            Link = answer.Link,
                        };
                        returnQuestion.Answers.Add(questionAnswer);
                    }
                    returnQuestionWithAnswers.Add(returnQuestion);
                }
            }
            return returnQuestionWithAnswers;
        }
    }
}