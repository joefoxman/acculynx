﻿using TestApi.Contracts;
using TestApi.Shared;

namespace TestApi.Services {
    public class WeatherService : IWeatherService, IServiceInterfaceBase {
        private string _url;
        private readonly IApiService _apiService;

        public WeatherService(IApiService apiService, CommonSettings commonSettings) {
            _apiService = apiService;
            _url = commonSettings.WeatherServiceUrl;
        }

        public async Task<Weather> GetData(string data) {
            if (string.IsNullOrWhiteSpace(data)) {
                data = "10001";
            }
            _url = _url.Replace("zipCode", data);

            var model = await _apiService.GetData<Weather>(_url);
            // this random delay similates a wait period
            var randomDelayMilliseconds = Helper.GetRandomDelayMilliseconds();
            Console.WriteLine($"Weather GetData Delayed by {randomDelayMilliseconds} milliseconds");
            await Task.Delay(randomDelayMilliseconds);
            return model;
        }
    }
}