﻿namespace TestApi.Shared {
    public class CommonSettings {
        public string GetLocationServiceUrl { get; set; } = string.Empty;
        public string WeatherServiceUrl { get; set; } = string.Empty;
        public string StackExchangeUrl { get; set; } = string.Empty;
        public string StackExchangeQuestionsUrl { get; set; } = string.Empty;
        public string StackExchangeQuestionUrl { get; set; } = string.Empty;
        public string StackExchangeAnswerUrl { get; set;} = string.Empty;
        public string StackExchangeQuestionWithAllAnswers { get; set; } = string.Empty;
    }
}