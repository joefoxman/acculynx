﻿using Newtonsoft.Json;

namespace TestApi.Shared.StackOverflow {
    public class AnswerInfo {
        [JsonProperty("is_accepted")]
        public bool IsAccepted { get; set; }
        public int Score { get; set; }

        [JsonProperty("last_activity_date")]
        public int LastActivityDate { get; set; }

        [JsonProperty("creation_date")]
        public int CreationDate { get; set; }

        [JsonProperty("answer_id")]
        public int AnswerId { get; set; }

        [JsonProperty("question_id")]
        public int QuestionId { get; set; }

        [JsonProperty("content_license")]
        public string ContentLicense { get; set; } = string.Empty;

        [JsonProperty("last_edit_date")]
        public int? LastEditDate { get; set; }

        public string Link { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;
    }
}
