﻿using Newtonsoft.Json;

namespace TestApi.Shared.StackOverflow {
    public class QuestionInfo {
        [JsonProperty("question_id")]
        public int QuestionId { get; set; }

        public string Link { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        
        [JsonProperty("is_answered")]
        public bool IsAnswered { get; set; }

        [JsonProperty("accepted_answer_id")]
        public int AcceptedAnswerId { get; set; }

        [JsonProperty("answer_count")]
        public int AnswerCount { get; set; }


        [JsonProperty("last_activity_date")]
        public int LastActivityDate { get; set; }

        [JsonProperty("creation_Date")]
        public int CreationDate { get; set; }

        public List<AnswerInfo>? Answers { get; set; }
    }
}
