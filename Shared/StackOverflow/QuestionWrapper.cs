﻿using Newtonsoft.Json;

namespace TestApi.Shared.StackOverflow {
    public class QuestionWrapper {
        public List<QuestionInfo>? Items { get; set; }

        [JsonProperty("has_more")]
        public bool HasMore { get; set; }

        [JsonProperty("quota_max")]
        public int QuotaMax { get; set; }

        [JsonProperty("quota_remaining")]
        public int QuotaRemaining { get; set; }
    }
}
