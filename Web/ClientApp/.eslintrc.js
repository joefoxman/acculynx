module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        'plugin:@typescript-eslint/recommended',
        '@vue/typescript'
    ],
    // required to lint *.vue files
    plugins: [
        'vue',
        '@typescript-eslint'
    ],
    parser: 'vue-eslint-parser',
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'module',
        allowImportExportEverywhere: true,
        ecmaFeatures: {
            jsx: true,
            modules: true
        }
    },
    rules: {
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-use-before-define': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/consistent-type-assertions': 'off',
        '@typescript-eslint/no-this-alias': 'off',
        '@typescript-eslint/no-unused-vars': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/interface-name-prefix': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        'no-empty': 'off',
        'no-var': 'off',
        'use-isnan': 'off',
        'no-useless-escape': 'off',
        'no-console': 'off',
        'no-debugger': 'off',
        'prefer-const': 'off',
        'vue/html-closing-bracket-newline': 'off',
        'vue/html-closing-bracket-spacing': 'off',
        'vue/html-indent': 'off',
        'vue/html-quotes': 'off',
        'vue/multiline-html-element-content-newline': 'off',
        'space-before-function-paren': 0
    }
};
