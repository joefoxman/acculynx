import { Common } from "./common";

export class ApiService {
    fetchGet<TResponse>(url: string): Promise<TResponse> {
        let requestOptions: RequestInit = Common.getRequestOptions("GET", null);
        requestOptions.method = "GET";

        return this.fetchInternal<TResponse>(url, requestOptions);
    }

    fetchWithBody<TBody, TResponse>(
        url: string,
        method: string,
        body: TBody,
        additionalHeaders?: Headers
        ): Promise<TResponse> {
        const availableMethods = ["POST", "PUT", "DELETE"];
    
        if (availableMethods.indexOf(method) == -1) {
            throw new Error("Method not supported");
        }
    
        let requestOptions: RequestInit = Common.getRequestOptions("POST", body);
        requestOptions.method = method;
        requestOptions.body = JSON.stringify(body);
    
        return this.fetchInternal<TResponse>(url, requestOptions);
    }
    
    async fetchInternal<TResponse>(url: string, requestOptions: RequestInit): Promise<TResponse> {
        try {
            const response = await fetch((process.env.VUE_APP_API_BASE_URL || '') + url, requestOptions);
            const statusText = await response.text();
            const responseObject = {
                status: response.status,
                text: statusText
            };
            let parsedResponse: TResponse;
            try {
                parsedResponse = JSON.parse(responseObject.text) as TResponse;
            } catch (error) {
                // notification to the user
                // use the swal
                parsedResponse = {} as TResponse;
            }
            return parsedResponse;
 
        } catch (error) {
            // notification to the user
            // use the swal
            throw error;
        }
    }
}

