import moment from "moment";
import _ from "lodash";
import { ReviewHeader } from "../viewmodels/Bor/Bor.viewmodel";
import swal from "sweetalert2";
import JQuery from "jquery";
import { UserRoles } from "../viewmodels/system/common.viewmodel";
let $ = JQuery;
export var UserRole;
(function (UserRole) {
    UserRole[UserRole["Admin"] = 1] = "Admin";
})(UserRole || (UserRole = {}));
export class Common {
    static getRequestOptions(method, model) {
        let requestOptions = {};
        if (model !== null) {
            requestOptions = {
                method: method,
                headers: { "Content-Type": "application/json; charset=utf-8" },
                body: JSON.stringify(model),
            };
        }
        else {
            requestOptions = {
                method: method,
            };
        }
        return requestOptions;
    }
    objectDifferences(object, base) {
        function changes(objectInner, baseInner) {
            return _.transform(objectInner, (result, value, key) => {
                if (!_.isEqual(value, baseInner[key])) {
                    result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, baseInner[key]) : value;
                }
            });
        }
        const returnChanges = changes(object, base);
        if (this.isEmptyObject(returnChanges)) {
            return null;
        }
        return returnChanges;
    }
    isAdmin(storeData) {
        let isAdmin = storeData.systemViewModel.loggedInRoles.filter(a => a === "Admin").length > 0;
        return isAdmin;
    }
    static isAVPOrHigher(data) {
        if (!data)
            return false;
        if (this.isAdmin(data)
            || this.isAVP(data)
            || this.isVP(data)
            || this.isSVP(data)) {
            return true;
        }
        return false;
    }
    static isVPOrHigher(data) {
        if (!data)
            return false;
        if (this.isAdmin(data)
            || this.isVP(data)
            || this.isSVP(data)) {
            return true;
        }
        return false;
    }
    static isSVPOrHigher(data) {
        if (!data)
            return false;
        if (this.isAdmin(data)
            || this.isSVP(data)) {
            return true;
        }
        return false;
    }
    static getRoleName(userRole) {
        for (const role of Object.keys(UserRole)) {
            if (role.toString() === userRole.toString()) {
                return UserRole[role];
            }
        }
    }
    static isAdmin(data) {
        if (!data)
            return false;
        let isInRole = false;
        var storDataWrapper = data;
        var systemViewModel = storDataWrapper.systemViewModel;
        let roleName = this.getRoleName(UserRole.Admin);
        systemViewModel.loggedInRoles.forEach((role) => {
            if (role.replace(" ", "").toLowerCase() === roleName.toLowerCase()) {
                isInRole = true;
            }
        });
        return isInRole;
    }
    static isAVP(data) {
        if (!data)
            return false;
        var returnVal = false;
        var storDataWrapper = data;
        var systemViewModel = storDataWrapper.systemViewModel;
        systemViewModel.loggedInRoles.forEach((role) => {
            if (role.replace(" ", "").toLowerCase() === "avp" ||
                role.replace(" ", "").toLowerCase() === "reliefavp") {
                returnVal = true;
            }
        });
        return returnVal;
    }
    static isVP(data) {
        if (!data)
            return false;
        var returnVal = false;
        var storDataWrapper = data;
        var systemViewModel = storDataWrapper.systemViewModel;
        systemViewModel.loggedInRoles.forEach((role) => {
            if (role.replace(" ", "").toLowerCase() === "vp" ||
                role.replace(" ", "").toLowerCase() === "reliefvp") {
                returnVal = true;
            }
        });
        return returnVal;
    }
    static isSVP(data) {
        if (!data)
            return false;
        var returnVal = false;
        var storDataWrapper = data;
        var systemViewModel = storDataWrapper.systemViewModel;
        systemViewModel.loggedInRoles.forEach((role) => {
            if (role.replace(" ", "").toLowerCase() === "svp" ||
                role.replace(" ", "").toLowerCase() === "reliefsvp") {
                returnVal = true;
            }
        });
        return returnVal;
    }
    getRoles(storeData, masterBor) {
        const userRoles = new UserRoles();
        // check the roles based on what the user has access to
        userRoles.isAdmin = storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "ADMIN").length > 0;
        userRoles.isBm = (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "BM").length > 0
            || storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF BM").length > 0)
            && this.branchAccess(storeData, masterBor);
        userRoles.isAvp = (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "AVP").length > 0
            || storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF AVP").length > 0)
            && this.districtAccess(storeData, masterBor);
        userRoles.isVp = (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "VP").length > 0
            || storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF VP").length > 0)
            && this.regionAccess(storeData, masterBor);
        userRoles.isSvp = (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "SVP").length > 0
            || storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF SVP").length > 0)
            && this.divisionAccess(storeData, masterBor);
        userRoles.canReopenReview = userRoles.isAdmin || userRoles.isAvp || userRoles.isVp || userRoles.isSvp;
        userRoles.canCreateReview = userRoles.canReopenReview;
        return userRoles;
    }
    branchAccess(storeData, masterBor) {
        if (masterBor === null || masterBor.branchNumber === null) {
            return false;
        }
        const branch = storeData.hierarchy.branches.filter((branch) => branch.branchNumber === masterBor.branchNumber);
        if (branch.length) {
            // if Relief BM then only if this branch matches their relief branch
            if (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF BM").length !== 0) {
                return (storeData.hierarchy.user.branchesReliefManager.filter((branch) => branch.branchNumber === masterBor.branchNumber).length !== 0);
            }
            return true;
        }
        return false;
    }
    districtAccess(storeData, masterBor) {
        if (masterBor === null || masterBor.districtNumber === null) {
            return false;
        }
        const district = storeData.hierarchy.districts.filter((district) => district.districtNumber === masterBor.districtNumber);
        if (district.length) {
            // if Relief AVP then only if this district matches their relief district
            if (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF AVP").length !== 0) {
                return (storeData.hierarchy.user.districtsReliefManager.filter((district) => district.districtNumber === masterBor.districtNumber).length !== 0);
            }
            return true;
        }
        return false;
    }
    regionAccess(storeData, masterBor) {
        if (masterBor === null || masterBor.regionNumber === null) {
            return false;
        }
        const region = storeData.hierarchy.regions.filter((region) => region.regionNumber === masterBor.regionNumber);
        if (region.length) {
            // if Relief VP then only if this region matches their relief regin
            if (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF VP").length !== 0) {
                return (storeData.hierarchy.user.regionsReliefManager.filter((region) => region.regionNumber === masterBor.regionNumber).length !== 0);
            }
            return true;
        }
        return false;
    }
    divisionAccess(storeData, masterBor) {
        if (masterBor === null || masterBor.divisionNumber === null) {
            return false;
        }
        const division = storeData.hierarchy.divisions.filter((division) => division.divisionNumber.toString() === masterBor.divisionNumber.toString());
        if (division.length) {
            // if Relief SVP then only if this division matches their relief division
            if (storeData.systemViewModel.loggedInRoles.filter((role) => role.toUpperCase() === "RELIEF SVP").length !== 0) {
                return (storeData.hierarchy.user.divisionsReliefManager.filter((division) => division.divisionNumber === masterBor.divisionNumber).length !== 0);
            }
            return true;
        }
        return false;
    }
    getCurrentDate() {
        const dt = new Date().toJSON().slice(0, 10).replace(/-/g, "-");
        return dt;
    }
    getMasterBorStatusShortName(status) {
        if (status === "U" || status === "Upcoming") {
            return "U";
        }
        if (status === "IP" || status === "In Progress") {
            return "IP";
        }
        if (status === "AP" || status === "Approval") {
            return "AP";
        }
        if (status === "C" || status === "Complete") {
            return "C";
        }
        return "U";
    }
    getMasterBorStatusLongName(status) {
        if (status === "U" || status === "Upcoming") {
            return "Upcoming";
        }
        if (status === "IP" || status === "In Progress") {
            return "In Progress";
        }
        if (status === "AP" || status === "Approval") {
            return "Approval";
        }
        if (status === "C" || status === "Complete") {
            return "Complete";
        }
        return "Upcoming";
    }
    getValidationErrorString(validationErrors) {
        if (validationErrors === null || validationErrors.length <= 0) {
            return "";
        }
        var delim = "";
        var returnString = "<div style='font-weight: bold; color: red;'>";
        validationErrors.forEach(((validationError) => {
            returnString += delim + "<p>" + validationError.errorMessage + "</p>";
            delim = "<p></p>";
        }));
        return returnString += "</div>";
    }
    noAccess(initialData) {
        const data = initialData;
        // if the user has not access end it here
        if (data.systemViewModel.noAccess) {
            swal.fire({
                title: "BOR Access",
                text: "You are not authorized to access this application.  Please contact the BOR administrator",
                icon: "error",
            }).then(() => { });
            return true;
        }
        return false;
    }
    static GetBrowser() {
        var test = function (regexp) { return regexp.test(window.navigator.userAgent); };
        switch (true) {
            case test(/edg/i): return "Microsoft Edge";
            case test(/trident/i): return "Microsoft Internet Explorer";
            case test(/firefox|fxios/i): return "Mozilla Firefox";
            case test(/opr\//i): return "Opera";
            case test(/ucbrowser/i): return "UC Browser";
            case test(/samsungbrowser/i): return "Samsung Browser";
            case test(/chrome|chromium|crios/i): return "Google Chrome";
            case test(/safari/i): return "Apple Safari";
            default: return "Other";
        }
    }
    ;
    static getReviewHeader(review) {
        const reviewHeader = new ReviewHeader();
        let lastReviewDate = "";
        if (review.lastCompletedReview !== null && review.lastCompletedReview.toString() !== "0001-01-01T00:00:00") {
            lastReviewDate = moment(review.lastCompletedReview.toString()).format("MM-DD-YYYY");
        }
        reviewHeader.reviewId = review.id;
        reviewHeader.status = review.status;
        reviewHeader.avp = review.districtManagerName;
        reviewHeader.branchName = review.branchName;
        reviewHeader.branchNumber = review.branchNumber;
        reviewHeader.districtNumber = review.districtNumber;
        reviewHeader.lastCompletedReview = lastReviewDate;
        reviewHeader.lastCompletedReviewId = review.lastCompletedReviewId;
        reviewHeader.dateEstimatedCompletion = moment(review.dateEstimatedCompletion).format("MM-DD-YYYY");
        reviewHeader.scheduledDate = moment(review.dateScheduled).format("MM-DD-YYYY");
        if (review.dateStarted !== null) {
            reviewHeader.startedDate = moment(review.dateStarted).format("MM-DD-YYYY");
            reviewHeader.reviewStartedByName = review.reviewStartedByName;
        }
        else {
            reviewHeader.startedDate = "";
            reviewHeader.reviewStartedByName = "";
        }
        if (review.dateCompleted !== null) {
            reviewHeader.completedDate = moment(review.dateCompleted).format("MM-DD-YYYY");
        }
        else {
            reviewHeader.completedDate = "";
        }
        reviewHeader.reviewType = review.reviewType;
        reviewHeader.branchManagerName = review.branchManagerName;
        reviewHeader.doesDateChangeRequestExist = review.doesDateChangeRequestExist;
        reviewHeader.requestDateChangeId = review.requestDateChangeId;
        reviewHeader.version = review.version;
        return reviewHeader;
    }
    static isEmpty(data) {
        if (data === undefined ||
            data === null ||
            (Array.isArray(data) && data.length === 0) ||
            (!Array.isArray(data) && ((typeof data === "string" || data instanceof String) && data.trim() === ""))) {
            return true;
        }
        return false;
    }
    isEmptyObject(obj) {
        for (let prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                return false;
            }
        }
        return true;
    }
    clone(objectToClone) {
        const clonedObject = _.cloneDeep(objectToClone);
        return clonedObject;
    }
    getGroupOfQuestions(dataEntryGroup, groupName) {
        return dataEntryGroup.filter(a => a.masterQuestion.questionText === groupName);
    }
    async delay(ms) {
        await new Promise(resolve => setTimeout(() => resolve(), ms));
    }
    setLoaders(state, maxLoaders, loaders) {
        if (loaders === undefined || loaders.length === 0) {
            loaders = [];
            for (let i = 0; i < maxLoaders; i++) {
                loaders.push(state);
            }
        }
        else {
            for (let i = 0; i < maxLoaders; i++) {
                loaders[i] = state;
            }
        }
        return loaders;
    }
    isValidDate(value) {
        const m = moment(value, "MM-DD-YYYY");
        return m.isValid();
    }
    convertToNumber(value) {
        try {
            const num = parseFloat(value);
            return num;
        }
        catch (ex) {
            return 0;
        }
    }
    toCurrency(value) {
        if (typeof value !== "number") {
            return 0;
        }
        const formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 2
        });
        return formatter.format(value);
    }
    ;
    unFormatTimeSpan(value) {
        if (value) {
            const dt = new Date("01/01/2019 " + value);
            const tempTime = moment(dt).format("HH:mm");
            return tempTime;
        }
        return value;
    }
    ;
    formatTimeSpan(value) {
        if (value) {
            const dt = new Date("01/01/2019 " + value);
            return moment(dt).format("h:mm A");
        }
        return value;
    }
    ;
    formatDateFieldForDatePickerNoTime(value) {
        if (value) {
            const dt = new Date(value);
            const formattedDate = moment.utc(dt).format('YYYY-MM-DD');
            return formattedDate;
        }
        return value;
    }
    compareDateBefore(dtTo, dtFrom) {
        var isBefore = moment(dtFrom).isBefore(dtTo);
        return isBefore;
    }
    compareDateAfter(dtTo, dtFrom) {
        var isAfter = moment(dtFrom).isAfter(dtTo);
        return isAfter;
    }
    getMonthFromDate(dt) {
        const month = 1 + (moment(dt, "YYYY-MM-DD").month());
        if (month < 10) {
            return "0" + month;
        }
        return month;
    }
    getYearFromDate(dt) {
        const year = (moment(dt, "YYYY-MM-DD").year());
        return year;
    }
    getMonthsDifference(startDate, endDate) {
        const monthDifference = moment(new Date(endDate).toUTCString()).diff(new Date(startDate).toUTCString(), "months", true);
        return Math.round(monthDifference);
    }
    getDateFirstOfMonth(dt) {
        let holdDate = dt;
        if (dt === null || dt === "") {
            holdDate = moment(new Date()).format("YYYY-MM-DD");
        }
        else {
            holdDate = moment(dt).format("YYYY-MM-DD");
        }
        const year = holdDate.substr(0, 4);
        const month = holdDate.substr(5, 2);
        const currentDateFirstOfMonth = month + "/01/" + year;
        return currentDateFirstOfMonth;
    }
    addMonthToDate(dt, howMany) {
        const dtToConvert = new Date(this.getDateFirstOfMonth(dt));
        const newDate = moment(dtToConvert).add(howMany, "months").format("YYYY-MM-DD");
        return newDate;
    }
    setCurrentDateToFirstOfMonth(currentDate) {
        if (currentDate === null || currentDate === "") {
            return this.getDateFirstOfMonth(moment(new Date()).format("YYYY-MM-DD"));
        }
        const currentDateFirstOfMonth = this.getDateFirstOfMonth(currentDate);
        return currentDateFirstOfMonth;
    }
    formatDateForComparison(value) {
        if (!value) {
            return "";
        }
        return moment(new Date(value)).format("YYYY-MM-DD");
    }
    formatCalendarDate(value) {
        if (!value) {
            return "";
        }
        return moment(new Date(value)).format("MM/DD/YYYY");
    }
    formatCalendarDateCustom(value, format) {
        if (!value) {
            return "";
        }
        const dt = new Date(value).toUTCString();
        return moment(dt).format(format);
    }
    formatDateField(value) {
        if (value) {
            const dt = new Date(value);
            const formattedDate = moment(dt).format("MM/DD/YYYY hh:mm:ss A");
            return formattedDate;
        }
        return value;
    }
    ;
    formatDateFieldAtTime(value) {
        if (value) {
            const dt = new Date(value);
            const formattedDate = moment(dt).format("MM/DD/YYYY @hh:mm:ss A");
            return formattedDate;
        }
        return value;
    }
    ;
    formatDateFieldAt(value) {
        if (value) {
            const dt = new Date(value);
            const formattedDate = moment(dt).format("MM/DD/YYYY @hh:mm:ss A");
            return formattedDate;
        }
        return value;
    }
    ;
    formatDateFieldNoTime(value) {
        if (value) {
            value = value.replace(/-/g, '\/').replace(/T.+/, '');
            const dt = new Date(value);
            const formattedDate = moment(dt).format("MM/DD/YYYY");
            return formattedDate;
        }
        return value;
    }
    ;
    formatNullabeDateFieldNoTime(value) {
        if (value !== null) {
            const dt = new Date(value);
            const formattedDate = moment(dt).format("MM/DD/YYYY");
            return formattedDate;
        }
        return "";
    }
    ;
    isDate(dtString) {
        let dateToCheck = moment(new Date(dtString));
        return dateToCheck.isValid();
    }
    formatDateFieldTimeZone(value) {
        if (value) {
            // EST DST is 240, without DST it is 300
            let serverOffset = 300;
            // create Date object for current location
            const dt = new Date(value);
            const jan = new Date(dt.getFullYear(), 0, 1).getTimezoneOffset();
            const jul = new Date(dt.getFullYear(), 6, 1).getTimezoneOffset();
            const isDst = Math.max(jan, jul) !== dt.getTimezoneOffset();
            if (isDst) {
                serverOffset = 240;
            }
            // convert to milliseconds
            // add local time zone offset
            // get UTC time in milliseconds
            const utcDate = dt.getTime() + (((dt.getTimezoneOffset() - serverOffset) * 60000) * -1);
            const formattedDate = moment(utcDate).format("MM/DD/YYYY h:mm A");
            return formattedDate;
        }
        return value;
    }
    toFixed(convertNumber, decimalPlaces) {
        const num = parseFloat(convertNumber).toFixed(decimalPlaces);
        return num;
    }
}
//# sourceMappingURL=common.js.map
