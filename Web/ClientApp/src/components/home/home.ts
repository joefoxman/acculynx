import { ApiService } from "../apiService";
import { Answer, QuestionWrapper } from "../viewmodels/stackexchange.viewmodel";
export default {
  components: {
  },
  props: {
  },
  data() {
    return {
      apiService: new ApiService() as ApiService,
      localWeatherData: '' as string,
      geoLocationData: '' as string,
      getLocalWeatherUrl: 'Test/GetLocalWeather?zipCode=' as string,
      getGeoLocationUrl: 'Test/GetGeoLocation' as string,
      getDataUrl: 'Test/GetData?zipCode=' as string,
      getDataParallelUrl: 'Test/GetDataParallel?zipCode=' as string,
      getStackExchangeUrl: 'StackExchange/GetQuestions' as string,
      questionWrapper: new QuestionWrapper() as QuestionWrapper,
      zipCode: '' as string,
      tab: null,
      showModal: false as boolean,
      answers: [] as Answer[]
    }
  },
  computed:{
  },
  watch:{
  },
  mounted() {
  },
  methods: {
    checkAnswer(item: Answer) {
      var correctAnswer = this.answers.find(a => a.isAccepted);
      if (correctAnswer == undefined) {
        alert("You did not pick the correct answer");
      }
      else {
        if (correctAnswer.answerId === item.answerId) {
          alert("BINGO, You did pick the correct answer");
        }
        else {
          alert("You did not pick the correct answer");
        }
      }
    },
    onClear() {
      this.localWeatherData = null;
      this.geoLocationData = null;
    },
    async showAnswers(questionId: number) { 
      this.answers = this.questionWrapper.find(a => a.questionId === questionId).answers;
      this.showModal = true;

    },
    async getStackExchange() {
      this.questionWrapper = await this.apiService.fetchGet(this.getStackExchangeUrl);
      var i = 0;
    },
    async getLocalWeather(){
      let query = this.getLocalWeatherUrl + this.zipCode.toString();
      this.localWeatherData = await this.apiService.fetchGet(query);
    },
    async getGeoLocation(){
      this.geoLocationData = await this.apiService.fetchGet(this.getGeoLocationUrl);
    },
    async getData() {
      let query = this.getDataUrl + this.zipCode.toString();
      let wrapper = await this.apiService.fetchGet(query);
      this.localWeatherData = wrapper.weather;
      this.geoLocationData = wrapper.geoLocation;
    },
    async getDataParallel() {
      let query = this.getDataParallelUrl + this.zipCode.toString();
      let wrapper = await this.apiService.fetchGet(query);
      this.localWeatherData = wrapper.weather;
      this.geoLocationData = wrapper.geoLocation;
    }
  }
}
