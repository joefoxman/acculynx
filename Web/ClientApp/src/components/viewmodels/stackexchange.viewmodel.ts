
export class QuestionWrapper {
  public questionId: number = 0
  public link: string = ''
  public title: string = ''
  public isAnswered: boolean = false
  public acceptedAnswerId: number = 0
  public answerCount: number = 0
  public answers: Answer[] = []
}

export class Answer {
  public isAccepted: boolean = false
  public score: number = 0
  public answerId: number = 0
  public questionId: number = 0
  public link: string = ''
  public title: string = ''
  public body: string = ''
}
