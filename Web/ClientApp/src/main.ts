import Vue from 'vue'
import router from './router'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = true
Vue.config.silent = true

new Vue({
    el: "#app-root",
    router,
    vuetify,
    render: h => h(require("./components/app/app.vue").default)
})
