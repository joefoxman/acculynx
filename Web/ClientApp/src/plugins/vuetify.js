"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("@fortawesome/fontawesome-free/css/all.css");
require("material-design-icons-iconfont/dist/material-design-icons.css");
var vue_1 = require("vue");
var vuetify_1 = require("vuetify");
var vue_js_toggle_button_1 = require("vue-js-toggle-button");
var vuetify_upload_button_1 = require("vuetify-upload-button");
var vue_multiselect_1 = require("vue-multiselect");
var vue_numeric_input_1 = require("vue-numeric-input");
var vue_numeric_1 = require("vue-numeric");
require("vuetify/dist/vuetify.min.css");
require("../css/site.css");
vue_1.default.use(vuetify_1.default);
vue_1.default.use(vue_numeric_1.default);
vue_1.default.use(vue_js_toggle_button_1.default);
vue_1.default.use(vuetify_upload_button_1.default);
vue_1.default.use(vue_numeric_input_1.default);
vue_1.default.component("multiselect", vue_multiselect_1.default);
exports.default = new vuetify_1.default({
    icons: {
        iconfont: 'fa' || 'md'
    }
});
//# sourceMappingURL=vuetify.js.map