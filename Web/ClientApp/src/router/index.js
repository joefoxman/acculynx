import Vue from 'vue';
import Router from 'vue-router';
import { StoreDataWrapper } from '../viewmodels/SystemViewModel.viewmodel';
import store from '../store';
import { Common } from '../components/common';
import Home from '../components/home/home.vue';
import ActionPlan from '../components/review/actionplan/actionPlan.vue';
import ActionPlaniFrame from '../components/iframe/actionplan/actionPlan.vue';
import PreAnalysis from '../components/review/preanalysis/preanalysis.vue';
import MiniBor from '../components/review/dataentry/minibor/dataentryminibor.vue';
import Profit from '../components/review/dataentry/profit/dataentryprofit.vue';
import Delinquency from '../components/review/dataentry/delinquency/dataentrydelinquency.vue';
import BusinessDevelopment from '../components/review/dataentry/businessdevelopment/dataentrybusinessdevelopment.vue';
import Personnel from '../components/review/dataentry/personnel/dataentrypersonnel.vue';
import InternalOperations from '../components/review/dataentry/internaloperations/dataentryinternaloperations.vue';
import FileReviews from '../components/review/filereviews/fileReviews.vue';
import Summary from '../components/review/summary/summary.vue';
import ReviewDateChangeStatus from '../components/review/information/reviewDateChangeStatus.vue';
import ReviewIndex from '../components/review/index.vue';
import Admin from '../components/admin/admin.vue';
import { ApiService } from '@/components/apiService';
import swal from "sweetalert2";
const CLIENT_VERSION = "BOR:clientVersion";
let clientVersion = window.localStorage.getItem(CLIENT_VERSION);
let stateLoaded = false;
let common = new Common();
Vue.use(Router);
const routes = [
    { path: "/", component: Home, name: 'home' },
    { path: '/admin', component: Admin, name: 'admin', props: true },
    { path: "/review/index/", component: ReviewIndex, name: 'reviewIndex',
        children: [
            { path: "/review/actionplan/", component: ActionPlan, props: true },
            { path: "/review/preanalysis/", component: PreAnalysis },
            { path: "/review/dataentry/minibor/", component: MiniBor, props: true },
            { path: "/review/dataentry/profit/", component: Profit, props: true },
            { path: "/review/dataentry/delinquency/", component: Delinquency, props: true },
            { path: "/review/dataentry/business/", component: BusinessDevelopment, props: true },
            { path: "/review/dataentry/personnel/", component: Personnel, props: true },
            { path: "/review/dataentry/internaloperations/", component: InternalOperations, props: true },
            { path: "/review/filereviews/", component: FileReviews, props: true },
            { path: "/review/summary/", component: Summary, props: true },
            { path: "/review/reviewdatechangestatus/", component: ReviewDateChangeStatus, props: true }
        ]
    },
    { path: "/review/actionplan/iframe", component: ActionPlaniFrame, props: true },
];
const router = new Router({
    routes,
    mode: 'history',
    base: process.env.BASE_URL
});
router.beforeEach(async (to, from, next) => {
    let apiService = new ApiService();
    var iFrameAccessOnly = to.path === "/review/actionplan/iframe";
    if (!stateLoaded) {
        if (Common.GetBrowser() !== "Google Chrome") {
            swal.fire({
                title: "Browser Warning",
                icon: "warning",
                html: "<div style='font-weight: bold; color: red;'><p>Please use the Chrome Browser for the best experience.</p></div>",
                showCloseButton: true,
            });
        }
        console.log("Loading Store Data...");
        let data = await apiService.fetchGet("/api/common/GetStoreData");
        var initialData = new StoreDataWrapper();
        initialData.systemViewModel = data.systemViewModel;
        iFrameAccessOnly = iFrameAccessOnly && initialData.systemViewModel.actionPlanAccessOnly;
        if (clientVersion === null ||
            clientVersion !== initialData.systemViewModel.version) {
            // write the server version now so they match
            window.localStorage.setItem(CLIENT_VERSION, initialData.systemViewModel.version);
            // reload now if they don't match
            if (clientVersion !== null && clientVersion !== initialData.systemViewModel.version) {
                console.log("reloading...");
                window.location.reload();
            }
        }
        initialData.hierarchy = data.hierarchy;
        store.commit("setInitialData", initialData);
        stateLoaded = true;
        console.log("Store Data Loaded");
        if (iFrameAccessOnly) {
            next();
        }
        else if (!common.noAccess(initialData)) {
            next();
        }
    }
    else {
        if (iFrameAccessOnly === true) {
            next();
        }
        if (common.noAccess(store.state.data)) {
            return false;
        }
        next();
    }
    return false;
});
export default router;
//# sourceMappingURL=index.js.map