using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestApi.Contracts;

namespace TestApi.WebApi.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class StackExchangeController : ControllerBase {
        private readonly IStackExchangeService _stackExchangeService;

        public StackExchangeController(
            IStackExchangeService stackExchangeService) {

            _stackExchangeService = stackExchangeService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("GetQuestions")]
        public async Task<IActionResult> GetQuestions() {
            var questions = await _stackExchangeService.GetAnswers();

            return Ok(questions);
        }
    }
}