﻿using System;

namespace Web.Middleware.Util {
    public class CompileErrorException : Exception {
        public CompileErrorException(string message) : base(message) {
        }
    }
}
