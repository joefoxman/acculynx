﻿using System;

namespace Web.Middleware.Util {
    public class TaskReturn {
        public Uri Uri { get; set; }
        public int Port { get; set; }
        public string Message { get; set; }
    }
}
